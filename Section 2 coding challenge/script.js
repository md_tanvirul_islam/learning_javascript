'use strict' ;


/*
 * JavaScript Fundamentals –Part 2 Section 3 Challenge 1 
 */

const calcAverage = (score1,score2,score3) => {return (score1+score2+score3)/3};
const checkWinner = (avgDolphins,avgKoalas) => {
   return avgDolphins > avgKoalas ? `Dolphins win (${avgDolphins} vs. ${avgKoalas})` : `Koalas win (${avgKoalas} vs. ${avgDolphins})` ;
}
/*
 * Section 3 Challenge 2
 */

const calcTip = function(bill){
   return bill < 1 ? "Please input a valid bill" : bill>50 && bill <300 ? bill*(15/100) : bill*(20/100);
}
const tips = [calcTip(125),calcTip(555),calcTip(44)];
const total =[125+calcTip(125),calcTip(555)+555,44+calcTip(44)]
console.log(tips,total);

/*
 * JavaScript Fundamentals –Part 2 Section 3 Challenge 2 
 */

const john = {
   'fullName' : 'John Smith',
   'weight' : 78,
   'height' : 1.69,
   'calcBMI' : function(){
   this.BMI = (this.weight / this.height)**2; 
     return this.BMI
   }
}

const mark = {
   'fullName' : 'Mark Miller',
   'weight' : 92,
   'height' : 1.95,
   'calcBMI' : function(){
   this.BMI = (this.weight / this.height)**2; 
     return this.BMI
   }
}

const testCalcBMI = ()=>{
   return ( 92/ 1.95)**2; 
}

console.log(testCalcBMI());

console.log(mark.fullName,mark.calcBMI(),mark.BMI);
console.log(john.fullName,john.calcBMI());


